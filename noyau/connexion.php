<?php

////////////////////////////
// ./noyau/connexion.php //
//////////////////////////


$dsn = "mysql:hosts=".DBHOTE.";dbname=".DBNAME;
$param = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');


try {
  $connexion = new PDO($dsn, DBUSER, DBPWD, $param);

} catch (PDOException $e) {
  echo "Erreur !" . $e->getMessage() . "<br/>";
}

 ?>
